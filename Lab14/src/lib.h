#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/** Константа, що вказує на кількість елементів в масиві - кількість студентів, що здали контрольну роботу */
#define STUDENT_THAT_ENDED_EXAM 3
/** Константа, що вказує на максимальну кількість символів у прізвищі студента */
#define STRINGS_INPUT_BUFER 30
/** Константа, що відображає максимальну оцінку за контрольну роботу, що може отримати студент */
#define MAX_MARK 100
/** Константа, яка відображає, скільки максимум символів може бути розташовано в 1 рядку у файлі */
#define BUFER 100

/** Структура, що представляє із себе склад контрольної роботи */
struct KontrolWorkStructure {
	int amount_of_practical_exercises;
	int amount_of_theoretical_questions;
	int amount_of_open_exercises;
};

/** Структура, яка представляє із себе контрольну роботу */
struct KontrolWork {
	int checked;
	char last_name[STRINGS_INPUT_BUFER];
	int mark;
	struct KontrolWorkStructure work_structure;
	char work_subject[STRINGS_INPUT_BUFER];
};

/** Функція, що бере данні з файлу
 * @param file_name Назва файлу
 * @param structure Структура, в яку буде записуватись результат */
void get_from_file(char *file_name, struct KontrolWork *structure);

/** Функція, що кладе данні в файл
 * @param file_name Назва файлу
 * @param structure Структура, в яка буде записана в файл */
void put_in_file(char *file_name, struct KontrolWork *structure);

/** Функція, для сортування контрольної роботи по кількості теоритичних запитань
 * @param structure структура, яка буде відсортирована
 * @param sorting_number кількість теоритичних питань, по якому буде проходити сортування
 * @return Відсортирована структура */
struct KontrolWork *sort_works(struct KontrolWork *structure,
			       int sorting_number);

/** Функція, для виводу массиву структур на єкран
 * @param structure Структура, яка буде виведена */
void print_structure(struct KontrolWork *structure);
