#include "lib.h"

double str_to_digit(const char *string)
{
	double result = 0;
	double fract = 1;
	bool pointed = false;

	for (int i = 0; (*(string + i) != '\0' && *(string + i) != '\n'); i++) {
		if (*(string + i) == '.' || *(string + i) == ',') {
			pointed = true;
			continue;
		}

		if (pointed == false) {
			result = (result * 10) + (*(string + i) - '0');
		} else {
			fract = fract / 10;
			result = result + (fract * (*(string + i) - '0'));
		}
	}

	return result;
}
