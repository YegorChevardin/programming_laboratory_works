#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

/**
 * Константа, що описує максимальну кількість строк, що може бути зчитана з файлу
 */
#define MAX_CHARACTERS 1000

/**
 * Функція, що конвертує подану строку у число
 * @param string Строка, яку треба конвертувати
 * @return число, яке було записано у строці
 */
double str_to_digit(const char *string);
