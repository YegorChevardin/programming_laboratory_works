/**
 * # Лабораторна робота номер 12
 * Строки (Null-terminated C Strings)
 * @author Chevardin Y.
 * @date 31-mar-2022
 * @version 1.0.0
 */

#include "lib.h"

/**
 * Точка входу
 * @return код завершення (0)
 */
int main(void)
{
	/* Ініціалізація змінних */
	char *bufer = malloc(MAX_CHARACTERS);
	double output = 0;

	/* Отримуємо ввід данних від користувача */
	printf("%s\n",
	       "Будльсака, введіть число, яке ви хочете конвертувати у тип int чи float: ");
	fgets(bufer, MAX_CHARACTERS, stdin);

	/* Конвертуемо строку в число */
	output = str_to_digit(bufer);

	/* Виводимо результат роботи функції */
	printf("Результат конвертації строки: %f\n", output);

	/* Звільняемо памьять */
	free(bufer);
	return 0;
}
