#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/** Константа, що описує розмір сітки й розмір буферу */
#define SIZE_OF_GRID 100

/**
 * Функція, що малює синусоїду із заданими параметрами
 * @param amplitude амплітуда синусоїди
 * @param period період синусоїди
 * @param grid сітка, на якій малюється рисунок
 * @param seeder символ, що ставиться
 */
void draw_sine_wave(int amplitude, int period, char seeder, int **grid);

/**
 * Функція, що ставить точку графіка у сітці у потрібному місті
 * @param x координата горизонталі
 * @param y координата вертикалі
 * @param grid сітка, на якій ставиться точка
 * @param seeder символ, що ставиться
 */
void plot(int x, int y, int **grid, char seeder);

/**
 * Функція, що виводить у консоль малюнок з символів
 * @param grid сітка, на якій нарисований малюнок
 */
void print_grid(int **grid);