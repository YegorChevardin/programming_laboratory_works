/**
 * # Лабораторна робота номер 9
 * 1. Зробити модульни тесті для программ, що були розроблені під час лабораторної роботи номер 7
 * @author Chevardin Y.
 * @date 14-feb-2022
 * @version 2.0.0
 */

#include "lib.h"
#include <check.h>

START_TEST(sqrt_test)
        {
                int number = 144;
                int actual = sqrt_from_number(number);
                int expected = 12;

                ck_assert_int_eq(actual, expected);
        }
END_TEST

START_TEST(multiple_matrix_test)
{
    int matrix[SIZE_OF_ROWS_AND_COLUMNS]
    [SIZE_OF_ROWS_AND_COLUMNS] = { { 2, 3, 4 },
                                   { 5, 4, 1 },
                                   { 0, 2, 3 } },
            matrix_string[SIZE_OF_ROWS_AND_COLUMNS *
                          SIZE_OF_ROWS_AND_COLUMNS],
            result_matrix[SIZE_OF_ROWS_AND_COLUMNS]
    [SIZE_OF_ROWS_AND_COLUMNS] = { { 0, 0, 0 },
                                   { 0, 0, 0 },
                                   { 0, 0, 0 } },
            expected_matrix[SIZE_OF_ROWS_AND_COLUMNS]
    [SIZE_OF_ROWS_AND_COLUMNS] = { { 19, 26, 23 },
                                   { 30, 33, 27 },
                                   { 10, 14, 11 } };

    for (int i = 0; i < SIZE_OF_ROWS_AND_COLUMNS; i++) {
        for (int j = 0; j < SIZE_OF_ROWS_AND_COLUMNS; j++) {
            matrix_string[i * SIZE_OF_ROWS_AND_COLUMNS + j] =
                    matrix[i][j];
        }
    }

    multiple_matrix(matrix_string, result_matrix);

    for (int i = 0; i < SIZE_OF_ROWS_AND_COLUMNS; i++) {
        for (int j = 0; j < SIZE_OF_ROWS_AND_COLUMNS; j++) {
            ck_assert_int_eq(result_matrix[i][j],
                             expected_matrix[i][j]);
        }
    }
}
END_TEST

int main(void)
{
    Suite *s = suite_create("Programming");
    TCase *tc_core = tcase_create("Lab09");

    tcase_add_test(tc_core, sqrt_test);
    tcase_add_test(tc_core, multiple_matrix_test);
    suite_add_tcase(s, tc_core);

    SRunner *sr = srunner_create(s);
    srunner_run_all(sr, CK_VERBOSE);
    int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
