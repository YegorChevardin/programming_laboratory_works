CC := clang
MAIN_EXE := main.bin
TEST_EXE := test.bin
EXE_DIR := bin
MAIN_SRC_DIR := src
TEST_SRC_DIR := test
MAIN_PATH := $(EXE_DIR)/$(MAIN_EXE)
TEST_PATH := $(EXE_DIR)/$(TEST_EXE)
LIB_OBJS  := lib.o
MAIN_OBJS := main.o
TEST_OBJS := test_all.o
CFLAGS    := -Og -g -fprofile-instr-generate -fcoverage-mapping
LDFLAGS   := -fprofile-instr-generate -fcoverage-mapping
TESTLIB   := -lcheck -lm -lrt -lpthread -lsubunit

.PHONY: all distclean clean

all: distclean format $(MAIN_PATH) $(TEST_PATH)
	./$(EXE_DIR)/$(MAIN_EXE)

$(MAIN_PATH): $(LIB_OBJS) $(MAIN_OBJS) | $(EXE_DIR)
	$(CC) $(LDFLAGS) $^ -o $@

$(TEST_PATH): $(LIB_OBJS) $(TEST_OBJS) | $(EXE_DIR)
	$(CC) $(LDFLAGS) $^ $(TESTLIB) -o $@

%.o: $(MAIN_SRC_DIR)/%.c
	$(CC) -c $(CFLAGS) $< -o $@
	
%.o: $(TEST_SRC_DIR)/%.c
	$(CC) -c $(CFLAGS) -I$(MAIN_SRC_DIR) $< -o $@
	
$(EXE_DIR):
	mkdir -p $@

test: $(TEST_PATH)
	-$(TEST_PATH)
	llvm-profdata merge -sparse default.profraw -o default.profdata
	llvm-cov report $(TEST_PATH) -instr-profile=default.profdata $(MAIN_SRC_DIR)/*.c
	llvm-cov show $(TEST_PATH) -instr-profile=default.profdata $(MAIN_SRC_DIR)/*.c -format html > coverage.html
	xdg-open coverage.html

distclean: clean
	@$(RM) -rv $(EXE_DIR) html coverage.html

clean:
	@$(RM) -v $(LIB_OBJS) $(MAIN_OBJS) $(TEST_OBJS) *.profdata *.profraw *.info

format: $(MAIN_SRC_DIR)/.clang-format
	clang-format $(MAIN_SRC_DIR)/* -i

docgen: Doxyfile
	doxygen Doxyfile
	xdg-open $(EXE_DIR)/html/index.html