#include "lib.h"

/* Multiple matrix section starts */
int multiple_matrix(
	int matrix_string[SIZE_OF_ROWS_AND_COLUMNS * SIZE_OF_ROWS_AND_COLUMNS],
	int result_matrix[SIZE_OF_ROWS_AND_COLUMNS][SIZE_OF_ROWS_AND_COLUMNS])
{
	for (int i = 0; i < SIZE_OF_ROWS_AND_COLUMNS; i++) {
		for (int j = 0; j < SIZE_OF_ROWS_AND_COLUMNS; j++) {
			result_matrix[i][j] = 0;
			for (int k = 0; k < SIZE_OF_ROWS_AND_COLUMNS; k++) {
				result_matrix[i][j] +=
					matrix_string
						[i * SIZE_OF_ROWS_AND_COLUMNS +
						 k] *
					matrix_string
						[k * SIZE_OF_ROWS_AND_COLUMNS +
						 j];
			}
		}
	}

	return 0;
}
/* Multiple matrix section ends */

/* Sqrt from number section starts */
double module(double number)
{
	if (number < 0) {
		number = number * (-1);
	}

	return number;
}

double sqr(double number)
{
	number = number * number;

	return number;
}

int sqrt_from_number(int given_number)
{
	double x0, x1, module_of_difference;
	int result = 0;

	x0 = given_number;
	x1 = 0.5 * (x0 + given_number / x0);
	module_of_difference = module(x0 - x1);

	while (module_of_difference >= ACCURACY * 2 ||
	       sqr(module_of_difference) >= ACCURACY * 2) {
		x0 = x1;
		x1 = 0.5 * (x0 + given_number / x0);
		module_of_difference = module(x0 - x1);
	}

	result = x0;
	return result;
}
/* Sqrt from number section ends */
