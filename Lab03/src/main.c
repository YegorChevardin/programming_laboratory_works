/**
 * # Загальне завдання
 * 1. Зробити программу, що буде обчислювати площу і периметр прямокутника
 * 2. Номер завдання з лабораторної роботи - 21
 * @author Chevardin Y.
 * @date 21-oct-2021
 * @version 1.2.2
 */

#include <math.h>
#include <stdio.h>

//Створюю функцію, котра буде розраховувати довжину сторони
float side_counter(int x1, int y1, int x2, int y2) {
  float side = sqrt(pow(x2-x1, 2) + pow(y2-y1, 2));

  return side;
}

//Функція для розрахунку площі прямокутника
float rect_perimetr_counter(float first_side, float second_side) {
  float perimetr = 2 * (first_side + second_side);

  return perimetr;
}

float rect_space_counter(float first_side, float second_side) {
  float space = first_side * second_side;

  return space;
}

//Точка входу у программу
int main() {
	  //Добаляю змінні для ввода координат
    int x1, y1, x2, y2;

    //Визначення сторін прямокутника
    float first_side = side_counter(x1, y1, x1, y2);
    float second_side = side_counter(x1, y2, x2, y2);

    //Визначення периметра
    float p = rect_perimetr_counter(first_side, second_side);

    //Визначення площі
    float s = rect_space_counter(first_side, second_side);

	  return 0;
}
