#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/**Константа, що описує кількість стовпчиків та колонок у масиві масивів*/
#define NUMBER_OF_COLS_AND_ROWS 3

/**
 * Функція, яка бере з матриці діагональ, та розташовує її у порядку зростання
 * @param matrix показник на матрицю, з котрої буде братися діагональ
 * @param diagonal_elements показник на масив, у котрий буде записуватися результат
 * */
void diagonal_function(int **matrix, int *diagonal_elements);