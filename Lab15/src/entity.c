#include "lib.h"

struct StudentsArrayContainer *init_dynamic_array(int size)
{
	struct StudentsArrayContainer *container =
		malloc(sizeof(struct StudentsArrayContainer));

	container->array = malloc(size * sizeof(struct KontrolWork));
	container->size = size;

	return container;
}

void insert(struct StudentsArrayContainer *container, int position,
	    struct KontrolWork *element)
{
	struct KontrolWork *new_array =
		malloc((container->size + 1) * sizeof(struct KontrolWork));

	if (position > container->size) {
		position = container->size;
	}

	memcpy(new_array, container->array,
	       position * sizeof(struct KontrolWork));
	memcpy(new_array + position, element, sizeof(struct KontrolWork));
	memcpy(new_array + position + 1, container->array + position,
	       (container->size - position) * sizeof(struct KontrolWork));

	free(container->array);
	container->array = new_array;
	container->size++;
}

void delete_item(struct StudentsArrayContainer *container, int position)
{
	if (container->size == 0)
		return;

	struct KontrolWork *new_array =
		malloc((container->size - 1) * sizeof(struct KontrolWork));

	if (position >= container->size)
		position = container->size - 1;

	memcpy(new_array, container->array,
	       position * sizeof(struct KontrolWork));
	memcpy(new_array + position, container->array + position + 1,
	       (container->size - position - 1) * sizeof(struct KontrolWork));

	free(container->array);
	container->array = new_array;
	container->size--;
}
